from sys import argv
alp = [' ','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v', 'w', 'x', 'y', 'z']
with open(argv[1], "r") as f:
    t = f.read()
    char, txt, index = "", "", 0
    for c in t:
        char += c
        if char == "0":
            index += 1
            char = ""
        elif char == "1":
            print(alp[index], end='')
            char = ""
        if index == 27:
            index = 0
